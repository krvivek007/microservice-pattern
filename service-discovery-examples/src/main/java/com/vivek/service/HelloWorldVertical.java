package com.vivek.service;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.servicediscovery.Record;
import io.vertx.servicediscovery.ServiceDiscovery;
import io.vertx.servicediscovery.ServiceDiscoveryOptions;
import io.vertx.servicediscovery.types.HttpEndpoint;

public class HelloWorldVertical extends AbstractVerticle {

  public static void main(String[] args) {
    Vertx vertx = Vertx.vertx();
    vertx.deployVerticle(HelloWorldVertical.class.getName());
  }

  @Override
  public void start() throws Exception {
    Router router = Router.router(vertx);
    router.get("/hello").handler(rc -> {
      rc.response()
        .putHeader("content-type", "application/json")
        .end(new JsonObject().put("greeting", "Hello World!").encode());
    });
    vertx.createHttpServer().requestHandler(router::accept).listen(8089);
    ServiceDiscovery discovery = ServiceDiscovery.create(vertx);
    Record record = HttpEndpoint.createRecord("hello-service", "localhost", 8089, "/hello");
    discovery.publish(record, ar -> {
      if (ar.succeeded()) {
        System.out.println("Successfully published service endpoint");
        vertx.deployVerticle(HelloWorldVerticalMessageConsumer.class.getName());
      } else {
        System.out.println("Error published service endpoint");
      }
    });
    discovery.close();
  }
}
