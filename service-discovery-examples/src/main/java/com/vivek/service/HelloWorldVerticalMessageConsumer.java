package com.vivek.service;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.Message;
import io.vertx.core.http.HttpClient;
import io.vertx.servicediscovery.Record;
import io.vertx.servicediscovery.ServiceDiscovery;
import io.vertx.servicediscovery.ServiceReference;
import io.vertx.servicediscovery.types.HttpEndpoint;

public class HelloWorldVerticalMessageConsumer extends AbstractVerticle {
    @Override
    public void start() throws Exception {
        ServiceDiscovery discovery = ServiceDiscovery.create(vertx);
        discovery.getRecord(r -> r.getName().equalsIgnoreCase("hello-service"), ar -> {
            System.out.println(ar.cause());
            if (ar.succeeded()) {
                if (ar.result() != null) {
                    ServiceReference reference = discovery.getReference(ar.result());
                    HttpClient client = reference.getAs(HttpClient.class);
                    client.getNow("/hello ", response -> {
                        if (response.statusCode() != 200) {
                            System.err.println("******" + "fail");
                        } else {
                            response.bodyHandler(b -> System.out.println("*******" + b.toString()));
                        }
                        reference.release();
                    });
                } else {
                    System.out.println("unable to get result");
                }
            } else {
                System.out.println("no success");
            }
            discovery.close();
        });
    }
}
